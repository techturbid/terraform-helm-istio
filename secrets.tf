resource "kubernetes_secret" "grafana" {
  depends_on = ["helm_release.istio_crd"]
  metadata {
    name = "grafana"
    namespace = "istio-system"
    labels = {
      app = "grafana"
    }
  }

  type = "kubernetes.io/opaque"

  data = {
    username = var.GRAFANA_USER
    passphrase = var.GRAFANA_PASSWORD
  }

}

resource "kubernetes_secret" "kiali" {
  depends_on = ["helm_release.istio_crd"]
  metadata {
    name = "kiali"
    namespace = "istio-system"
    labels = {
      app = "kiali"
    }
  }

  type = "kubernetes.io/opaque"

  data = {
    username = var.KIALI_USER
    passphrase = var.KIALI_PASSWORD
  }

}
