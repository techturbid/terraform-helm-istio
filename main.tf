resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}

data "google_dns_managed_zone" "techtur-bid" {
  depends_on = [null_resource.wait_for_resources]
  project    = var.GCP_DNS_PROJECT
  name       = var.DNS_MANAGED_ZONE
}

locals {
  cluster_domain = "${var.CLUSTER_NAME}.${var.TECHTURBID_DOMAIN_NAME}"
}

resource "null_resource" "get_istio" {
  triggers = {
    always_run = "${timestamp()}"
  }
  depends_on = ["null_resource.wait_for_resources"]
  provisioner "local-exec" {
    command = "curl -L https://git.io/getLatestIstio | ISTIO_VERSION=${var.istio_version} sh"
  }
}

resource "helm_release" "istio_crd" {
  depends_on = ["null_resource.get_istio"]
  name       = "istio-init"
  chart      = "./istio-${var.istio_version}/install/kubernetes/helm/istio-init"

  version   = var.istio_version
  namespace = "istio-system"
}

resource "null_resource" "wait_istio_crd" {
  depends_on = ["helm_release.istio_crd"]
  provisioner "local-exec" {
    command = "sleep 60"
  }
}
resource "helm_release" "istio" {
  depends_on = ["null_resource.get_istio", "null_resource.wait_istio_crd", "kubernetes_secret.grafana", "kubernetes_secret.kiali"]
  name       = "istio"
  chart      = "./istio-${var.istio_version}/install/kubernetes/helm/istio"
  version    = var.istio_version
  namespace  = "istio-system"
  
//  set {
//    name  = "gateways.istio-ingressgateway.sds.enabled"
//    value = "true"
//  }
//  set {
//    name  = "global.k8sIngress.enabled"
//    value = "true"
//  }
//  set {
//    name  = "global.k8sIngress.enableHttps"
//    value = "true"
//  }
//  set {
//    name  = "global.k8sIngress.gatewayName"
//    value = "ingressgateway"
//  }
  //  set {
  //    name = "certmanager.enabled"
  //    value = "true"
  //  }
  //  set {
  //    name = "certmanager.email"
  //    value = "techturbid@pm.me"
  //  }
  set {
    name  = "prometheus.retention"
    value = "72h"
  }
  //  set {
  //    name = "prometheus.ingress.enabled"
  //    value = "true"
  //  }
  //  set {
  //    name = "prometheus.ingress.hosts"
  //    value = "prometheus.${var.cluster_domain}"
  //  }
  set {
    name  = "grafana.enabled"
    value = "true"
  }
  set {
    name  = "grafana.security.enabled"
    value = "true"
  }
  //  set {
  //    name = "grafana.ingress.enabled"
  //    value = "true"
  //  }
  //  set {
  //    name = "grafana.ingress.hosts"
  //    value = "grafana.${var.cluster_domain}"
  //  }
  set {
    name  = "kiali.enabled"
    value = "true"
  }
  set {
    name  = "kiali.dashboard.grafanaURL"
    value = "https://grafana.${local.cluster_domain}"
  }
}
