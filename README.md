Techturbid Terraform Registry - istio
---
#### Istio module used by Techturbid's GKE modules like gke_base

By default this module will download and install istio based on the variable version using Helm

It will also configure Prometheus to gather information of the Service Mesh and install Kiali and Grafana for visualization 
 
---

The input required for this modules are:

|            **Variable**           |       **Value**       |
|-----------------------------------|:----------------------|
| CLUSTER_NAME                      |                       |
| DNS_MANAGED_ZONE                      |                       |
| GRAFANA_USER                      |                       |
| GRAFANA_PASSWORD                      |                       |
| KIALI_USER                      |                       |
| KIALI_PASSWORD                      |                       |

--- 
 
The variables with default values are:

|             **Variable**           |       **Value**       |
|------------------------------------|:----------------------|
| istio_version          | 1.3.2      |
| GCP_DNS_PROJECT          | techtur-bid-commons      |

---

This module will output the following values:

|              **Output**            |                       **Value**               |
|------------------------------------|:----------------------------------------------|
| istio_id                   | helm_release.istio.id               |

---

This module requires the following providers:
```hcl-terraform
provider "google" {
  credentials = var.service-account_key
  project     = var.gcp_project
  region      = var.cluster_region
}

provider "kubernetes" {
  load_config_file = false
  host = "https://${module.gke.cluster_endpoint}"
  cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
  token = data.google_client_config.google-beta_current.access_token
}

provider "helm" {
  install_tiller  = true
  tiller_image    = "gcr.io/kubernetes-helm/tiller:v${var.tiller_version}"
  service_account = var.kubernetes_service_account_name
  namespace       = var.kubernetes_service_account_namespace

  kubernetes {
    load_config_file = false
    host = "https://${module.gke.cluster_endpoint}"
    cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
    token = data.google_client_config.google-beta_current.access_token
  }
}
```

---