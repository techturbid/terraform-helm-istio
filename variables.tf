variable "wait_for_resources" {}
variable "istio_version" {
  description = "Istio version"
}
variable "DNS_MANAGED_ZONE" {}
variable "GCP_DNS_PROJECT" {}
variable "TECHTURBID_DOMAIN_NAME" {}
variable "CLUSTER_NAME" {}
variable "GRAFANA_USER" {}
variable "GRAFANA_PASSWORD" {}
variable "KIALI_USER" {}
variable "KIALI_PASSWORD" {}
